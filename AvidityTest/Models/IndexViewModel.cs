﻿using System.Collections.Generic;

namespace AvidityTest.Models
{
    public class IndexViewModel
    {
        public string CharacterName { get; set; }

        public string MarvelAttributionText { get; set; }

        public string StoryName { get; set; }

        public string StoryDescription { get; set; }

        public List<CharacterImageViewModel> CharactersList { get; set; }

        public string ErrorMessage { get; set; } = "";
    }
}