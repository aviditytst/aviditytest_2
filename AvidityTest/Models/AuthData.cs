﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AvidityTest.Models
{
    public class AuthData
    {
        public string Ts { get; set; }

        public string ApiKey { get; set; }

        public string Hash { get; set; }
    }
}