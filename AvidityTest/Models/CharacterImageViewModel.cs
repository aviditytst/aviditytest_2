﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AvidityTest.Models
{
    public class CharacterImageViewModel
    {
        public string Name { get; set; }

        public string ImagePath { get; set; }
    }
}