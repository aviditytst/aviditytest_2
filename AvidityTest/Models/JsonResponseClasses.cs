﻿using System;
using System.Collections.Generic;

namespace AvidityTest.Models
{
    public class Item
    {
        public string resourceURI { get; set; }
        public string name { get; set; }
        public string role { get; set; }
    }

    public class Creators
    {
        public int available { get; set; }
        public string collectionURI { get; set; }
        public List<Item> items { get; set; }
        public int returned { get; set; }
    }

    public class CharactersItem
    {
        public string resourceURI { get; set; }
        public string name { get; set; }
    }

    public class Characters
    {
        public int available { get; set; }
        public string collectionURI { get; set; }
        public List<CharactersItem> items { get; set; }
        public int returned { get; set; }
    }

    public class SeriesItem
    {
        public string resourceURI { get; set; }
        public string name { get; set; }
    }

    public class Series
    {
        public int available { get; set; }
        public string collectionURI { get; set; }
        public List<SeriesItem> items { get; set; }
        public int returned { get; set; }
    }

    public class ComicsItem
    {
        public string resourceURI { get; set; }
        public string name { get; set; }
    }

    public class Comics
    {
        public int available { get; set; }
        public string collectionURI { get; set; }
        public List<ComicsItem> items { get; set; }
        public int returned { get; set; }
    }

    public class Events
    {
        public int available { get; set; }
        public string collectionURI { get; set; }
        public List<EventsItem> items { get; set; }
        public int returned { get; set; }
    }

    public class OriginalIssue
    {
        public string resourceURI { get; set; }
        public string name { get; set; }
    }

    public class StoriesResult
    {
        public int id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string resourceURI { get; set; }
        public string type { get; set; }
        public DateTime modified { get; set; }
        public object thumbnail { get; set; }
        public Creators creators { get; set; }
        public Characters characters { get; set; }
        public Series series { get; set; }
        public Comics comics { get; set; }
        public Events events { get; set; }
        public OriginalIssue originalIssue { get; set; }
    }

    public class StoriesData
    {
        public int offset { get; set; }
        public int limit { get; set; }
        public int total { get; set; }
        public int count { get; set; }
        public List<StoriesResult> results { get; set; }
    }

    public class StoriesRoot
    {
        public int code { get; set; }
        public string status { get; set; }
        public string copyright { get; set; }
        public string attributionText { get; set; }
        public string attributionHTML { get; set; }
        public string etag { get; set; }
        public StoriesData data { get; set; }
    }

    public class Thumbnail
    {
        public string path { get; set; }
        public string extension { get; set; }
    }

    public class StoriesItem
    {
        public string resourceURI { get; set; }
        public string name { get; set; }
        public string type { get; set; }
    }

    public class Stories
    {
        public int available { get; set; }
        public string collectionURI { get; set; }
        public List<StoriesItem> items { get; set; }
        public int returned { get; set; }
    }

    public class EventsItem
    {
        public string resourceURI { get; set; }
        public string name { get; set; }
    }

    public class MarvelCharacterUrl
    {
        public string type { get; set; }
        public string url { get; set; }
    }

    public class CharacterResult
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime modified { get; set; }
        public Thumbnail thumbnail { get; set; }
        public string resourceURI { get; set; }
        public Comics comics { get; set; }
        public Series series { get; set; }
        public Stories stories { get; set; }
        public Events events { get; set; }
        public List<MarvelCharacterUrl> urls { get; set; }
    }

    public class CharacterData
    {
        public int offset { get; set; }
        public int limit { get; set; }
        public int total { get; set; }
        public int count { get; set; }
        public List<CharacterResult> results { get; set; }
    }

    public class MarvelCharacter
    {
        public int code { get; set; }
        public string status { get; set; }
        public string copyright { get; set; }
        public string attributionText { get; set; }
        public string attributionHTML { get; set; }
        public string etag { get; set; }
        public CharacterData data { get; set; }
    }
}