﻿using System.Threading.Tasks;
using System.Web.Mvc;
using AvidityTest.Interfaces;
using AvidityTest.Models;
using AvidityTest.Services;

namespace AvidityTest.Controllers
{
    public class HomeController : Controller
    {
        private IAuthDataService AuthDataService;
        private IMarvelApiClientService ApiClient;
        private IConfigReaderService ConfigReader;

        public async Task<ActionResult> Index()
        {
            AuthDataService = new AuthDataService();
            ApiClient = new MarvelApiClient(AuthDataService);
            ConfigReader = new ConfigReaderService();
            var configData = ConfigReader.GetConfigData();

            IndexViewModel htmlModel = await ApiClient.GetStories(configData);

            return View(htmlModel);
        }
    }
}