﻿using AvidityTest.Models;

namespace AvidityTest.Interfaces
{
    public interface IConfigReaderService
    {
        ConfigData GetConfigData();
    }
}
