﻿using System.Security.Cryptography;
using AvidityTest.Models;

namespace AvidityTest.Interfaces
{
    public interface IAuthDataService
    {
        AuthData GetAuthData(string key1, string key2);

        string GetMd5(string input);
    }
}
