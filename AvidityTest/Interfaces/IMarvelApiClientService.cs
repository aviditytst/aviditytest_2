﻿using AvidityTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvidityTest.Interfaces
{
    public interface IMarvelApiClientService
    {
        Task<IndexViewModel> GetStories(ConfigData authData);
        Task<string> BuildCharacterImage(CharacterImageViewModel imgModel, AuthData authData);
    }
}
