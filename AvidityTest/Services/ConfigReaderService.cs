﻿using System.Configuration;
using AvidityTest.Interfaces;
using AvidityTest.Models;
using System.Security.Cryptography;
using System.Text;
using System;
using System.Collections.Specialized;

namespace AvidityTest.Services
{
    public class ConfigReaderService : IConfigReaderService
    {
        public ConfigData GetConfigData()
        {
            ConfigData configData = new ConfigData();
            configData.CharacterName = ConfigurationManager.AppSettings["CharacterName"];
            configData.CharacterId = ConfigurationManager.AppSettings["CharacterId"];
            configData.Key1 = ConfigurationManager.AppSettings["key"];

            NameValueCollection section = (NameValueCollection)ConfigurationManager.GetSection("appSettings2");
            configData.Key2 = section["key2"];

            return configData;
        }

        public string GetMd5(MD5 md5Hash, string input)
        {
            var hashBytes = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            var result = new StringBuilder();

            for (int i = 0; i < hashBytes.Length; i++)
            {
                result.Append(hashBytes[i].ToString("x2"));
            }

            return result.ToString();
        }
    }
}