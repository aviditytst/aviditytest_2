﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using AvidityTest.Models;
using Newtonsoft.Json;
using AvidityTest.Interfaces;

namespace AvidityTest.Services
{
    public class MarvelApiClient : IMarvelApiClientService
    {
        private string UriCharacters = "https://gateway.marvel.com:443/v1/public/characters";

        private string UriStories = "https://gateway.marvel.com:443/v1/public/stories";

        private readonly IAuthDataService AuthDataService;

        private string ImageSize = "portrait_xlarge";

        public MarvelApiClient(IAuthDataService authDataService)
        {
            AuthDataService = authDataService;
        }

        public async Task<IndexViewModel> GetStories(ConfigData configData)
        {            
            var authData = AuthDataService.GetAuthData(configData.Key1, configData.Key2);
            var uriGetStories = $"{UriStories}?characters={configData.CharacterId}&ts={authData.Ts}&apikey={authData.ApiKey}&hash={authData.Hash}";
            var pageModel = new IndexViewModel();

            pageModel.CharacterName = configData.CharacterName;

            try
            {
                using (var client = new HttpClient())
                {
                    using (var response = await client.GetAsync(uriGetStories))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var resp = await response.Content.ReadAsStringAsync();
                            StoriesRoot stories = JsonConvert.DeserializeObject<StoriesRoot>(resp);

                            if (stories.data.results.Count > 0)
                            {
                                var storyResult = stories.data.results[0];
                                pageModel.StoryName = storyResult.title;
                                pageModel.StoryDescription = storyResult.description == "" ? "This story has no description" : storyResult.description;
                                pageModel.MarvelAttributionText = stories.attributionText;
                                var imgPath = "";
                                CharacterImageViewModel imgModel;

                                pageModel.CharactersList = new List<CharacterImageViewModel>();
                                foreach (var ch in storyResult.characters.items)
                                {
                                    imgModel = new CharacterImageViewModel
                                    {
                                        Name = ch.name
                                    };

                                    authData = AuthDataService.GetAuthData(configData.Key1, configData.Key2);
                                    imgPath = await BuildCharacterImage(imgModel, authData);
                                    imgModel.ImagePath = imgPath;
                                    pageModel.CharactersList.Add(imgModel);
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception)
            {
                pageModel.ErrorMessage = "Error occurred when loading the page.";
            }

            return pageModel;
        }

        public async Task<string> BuildCharacterImage(CharacterImageViewModel imgModel, AuthData authData)
        {
            var uriGetCharacter = $"{UriCharacters}?name={HttpUtility.HtmlEncode(imgModel.Name)}&ts={authData.Ts}&apikey={authData.ApiKey}&hash={authData.Hash}";
            var resultImgModel = new CharacterImageViewModel
            {
                Name = imgModel.Name
            };
            var imgPath = "";

            using (var client = new HttpClient())
            {
                using (var response = await client.GetAsync(uriGetCharacter))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var resp = await response.Content.ReadAsStringAsync();
                        MarvelCharacter charObj = JsonConvert.DeserializeObject<MarvelCharacter>(resp);

                        if (charObj.data.results.Count > 0)
                        {
                            var characResult = charObj.data.results[0];
                            imgPath = $"{characResult.thumbnail.path}//{ImageSize}.{characResult.thumbnail.extension}";
                        }
                    }
                }
            }

            return imgPath;
        }
    }
}