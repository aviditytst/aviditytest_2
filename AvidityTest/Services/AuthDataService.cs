﻿using System;
using System.Security.Cryptography;
using System.Text;
using AvidityTest.Interfaces;
using AvidityTest.Models;

namespace AvidityTest.Services
{
    public class AuthDataService : IAuthDataService
    {
        public AuthData GetAuthData(string key1, string key2)
        {
            var random = new Random();
            var randomTs = random.Next(1, 100);
            var ts = randomTs.ToString();
            var hash = GetMd5($"{ts}{key2}{key1}"); ;

            var authData = new AuthData
            {
                Ts = ts,
                ApiKey = key1,
                Hash = hash
            };

            return authData;
        }

        public string GetMd5(string input)
        {
            var result = new StringBuilder();

            using (MD5 md5Hash = MD5.Create())
            {
                var hashBytes = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

                for (int i = 0; i < hashBytes.Length; i++)
                {
                    result.Append(hashBytes[i].ToString("x2"));
                }
            }

            return result.ToString();
        }
    }
}